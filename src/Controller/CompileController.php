<?php
namespace Less\Controller;

use Less\Controller\AppController;

/**
 * Compile Controller
 *
 */
class CompileController extends AppController
{

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        if(isset($_REQUEST['less'])) {
            $this->response->type('text/css');
            $this->response->charset('UTF-8');
            $this->response->body($this->compile($_REQUEST['less']));
        }
        $this->autoRender = false;
    }

    public function compile($input) {
        require_once ROOT.'/vendor/leafo/lessphp/lessc.inc.php';
        $compiler = new \lessc();
        $compiledLess = $compiler->compile($input);
        return $compiledLess;
    }
}
