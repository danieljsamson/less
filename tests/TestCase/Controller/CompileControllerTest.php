<?php
namespace Less\Test\TestCase\Controller;

use Cake\TestSuite\IntegrationTestCase;
use Less\Controller\CompileController;

/**
 * Less\Controller\CompileController Test Case
 */
class CompileControllerTest extends IntegrationTestCase
{

    /**
     * Test compile
     *
     * @return void
     */
    public function testCompileValidCode()
    {
        $Compile = new \Less\Controller\CompileController();
        $expected =
'.block {\n  padding: 7px;\n}';
        $this->assertTextEquals(
            $Compile->compile('.block { padding: 3 + 4px }'),
            $expected
            );
    }
}
